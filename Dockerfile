FROM jenkins/jenkins:centos
ENV JAVA_OPTS "-Djenkins.install.runSetupWizard=false -Dpermissive-script-security.enabled=true -Dfile.encoding=UTF8"
ENV CASC_JENKINS_CONFIG="/usr/share/jenkins/ref/casc_configs"
USER 0
RUN yum -y install epel-release; \
    yum -y install python-pip;
#RUN yum groupinstall -y "development tools"

RUN pip install ansible==2.8.5 && ansible --version
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
COPY casc_configs ${CASC_JENKINS_CONFIG}

COPY locale.xml /usr/share/jenkins/ref/locale.xml
COPY .groovy /usr/share/jenkins/ref/.groovy

USER 1000
RUN git config --global user.email "jenkins@example.com" && \
  git config --global user.name "jenkins"
#COPY locale.xml /var/jenkins_home/locale.xml
#COPY .groovy  /var/jenkins_home/.groovy
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
